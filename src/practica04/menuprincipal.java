
package practica04;

public class menuprincipal extends javax.swing.JFrame {

    /**
     * Creates new form menuprincipal
     */
    public menuprincipal() {
        initComponents();
        ventana();
    }
    public void ventana (){
        setTitle("Menu Principal");
        setLocationRelativeTo(null);
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton3 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        btnEjercicio01 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton3.setFont(new java.awt.Font("Ebrima", 2, 14)); // NOI18N
        jButton3.setText("Ejercicio-01");
        getContentPane().add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 200, 120, 30));

        jButton2.setFont(new java.awt.Font("Ebrima", 2, 14)); // NOI18N
        jButton2.setText("Ejercicio-01");
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 170, 120, 30));

        jButton4.setFont(new java.awt.Font("Ebrima", 2, 14)); // NOI18N
        jButton4.setText("Ejercicio-01");
        getContentPane().add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 140, 120, 30));

        jButton5.setFont(new java.awt.Font("Ebrima", 2, 14)); // NOI18N
        jButton5.setText("Ejercicio-01");
        getContentPane().add(jButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 170, 120, 30));

        jButton6.setFont(new java.awt.Font("Ebrima", 2, 14)); // NOI18N
        jButton6.setText("Ejercicio-01");
        getContentPane().add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 230, 120, 30));

        btnEjercicio01.setFont(new java.awt.Font("Ebrima", 2, 14)); // NOI18N
        btnEjercicio01.setText("Ejercicio-01");
        getContentPane().add(btnEjercicio01, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 140, 120, 30));

        jButton8.setFont(new java.awt.Font("Ebrima", 2, 14)); // NOI18N
        jButton8.setText("Ejercicio-01");
        getContentPane().add(jButton8, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 200, 120, 30));

        jButton9.setFont(new java.awt.Font("Ebrima", 2, 14)); // NOI18N
        jButton9.setText("Ejercicio-01");
        getContentPane().add(jButton9, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 260, 120, 30));

        jButton10.setFont(new java.awt.Font("Ebrima", 2, 14)); // NOI18N
        jButton10.setText("Ejercicio-01");
        getContentPane().add(jButton10, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 230, 120, 30));

        jButton11.setFont(new java.awt.Font("Ebrima", 2, 14)); // NOI18N
        jButton11.setText("Ejercicio-01");
        getContentPane().add(jButton11, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 260, 120, 30));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/_Imagenes/diseñoHDS.png"))); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(menuprincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(menuprincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(menuprincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(menuprincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new menuprincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEjercicio01;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
}
