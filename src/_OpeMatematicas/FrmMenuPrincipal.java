package _OpeMatematicas;

public class FrmMenuPrincipal extends javax.swing.JFrame {

    public FrmMenuPrincipal() {
        initComponents();
        ventana();
    }
    public void ventana(){
        this.setLocationRelativeTo(null);
        this.setTitle("Menu Principal - Operaciones Matemáticas");
        //this.setSize(600, 300);
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btncuadrado = new javax.swing.JButton();
        btntriangulo = new javax.swing.JButton();
        btncirculo = new javax.swing.JButton();
        btncircunferencia = new javax.swing.JButton();
        btnrectangulo = new javax.swing.JButton();
        btntrapecio = new javax.swing.JButton();
        btnminimizar = new javax.swing.JButton();
        btncerrar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(0, 0, 102));

        jLabel1.setFont(new java.awt.Font("Ebrima", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Fórmulas Geométricas");

        btncuadrado.setFont(new java.awt.Font("Geometr212 BkCn BT", 0, 18)); // NOI18N
        btncuadrado.setMnemonic('C');
        btncuadrado.setText("Cuadrado");
        btncuadrado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncuadradoActionPerformed(evt);
            }
        });

        btntriangulo.setFont(new java.awt.Font("Geometr212 BkCn BT", 0, 18)); // NOI18N
        btntriangulo.setMnemonic('T');
        btntriangulo.setText("Triángulo");
        btntriangulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntrianguloActionPerformed(evt);
            }
        });

        btncirculo.setFont(new java.awt.Font("Geometr212 BkCn BT", 0, 18)); // NOI18N
        btncirculo.setMnemonic('C');
        btncirculo.setText("Circulo");
        btncirculo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncirculoActionPerformed(evt);
            }
        });

        btncircunferencia.setFont(new java.awt.Font("Geometr212 BkCn BT", 0, 18)); // NOI18N
        btncircunferencia.setMnemonic('C');
        btncircunferencia.setText("Circunferencia");
        btncircunferencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncircunferenciaActionPerformed(evt);
            }
        });

        btnrectangulo.setFont(new java.awt.Font("Geometr212 BkCn BT", 0, 18)); // NOI18N
        btnrectangulo.setMnemonic('R');
        btnrectangulo.setText("Rectangulo");
        btnrectangulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnrectanguloActionPerformed(evt);
            }
        });

        btntrapecio.setFont(new java.awt.Font("Geometr212 BkCn BT", 0, 18)); // NOI18N
        btntrapecio.setMnemonic('T');
        btntrapecio.setText("Trapecio");
        btntrapecio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntrapecioActionPerformed(evt);
            }
        });

        btnminimizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/_Imagenes/Minimize Window_45px.png"))); // NOI18N
        btnminimizar.setBorder(null);
        btnminimizar.setBorderPainted(false);
        btnminimizar.setContentAreaFilled(false);
        btnminimizar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnminimizar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnminimizar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/_Imagenes/Minimize Window_50px.png"))); // NOI18N
        btnminimizar.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnminimizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnminimizarActionPerformed(evt);
            }
        });

        btncerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/_Imagenes/Close Window_45px.png"))); // NOI18N
        btncerrar.setBorder(null);
        btncerrar.setBorderPainted(false);
        btncerrar.setContentAreaFilled(false);
        btncerrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btncerrar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btncerrar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/_Imagenes/Close Window_50px.png"))); // NOI18N
        btncerrar.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btncerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncerrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btncirculo, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btntriangulo, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btncuadrado, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnrectangulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btntrapecio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btncircunferencia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(27, 27, 27))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnminimizar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btncerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 383, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(39, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnminimizar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btncerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btncuadrado, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnrectangulo, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btntriangulo, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btntrapecio, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btncirculo, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btncircunferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(38, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 310));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnrectanguloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnrectanguloActionPerformed
        frmrectangulo rect = new frmrectangulo();
        rect.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btnrectanguloActionPerformed

    private void btncuadradoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncuadradoActionPerformed
        frmcuadrado cua = new frmcuadrado();
        cua.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btncuadradoActionPerformed

    private void btncerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncerrarActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btncerrarActionPerformed

    private void btnminimizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnminimizarActionPerformed
        // Boton Minimizar
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_btnminimizarActionPerformed

    private void btntrianguloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntrianguloActionPerformed
        frmtriangulo tri = new frmtriangulo();
        tri.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btntrianguloActionPerformed

    private void btncirculoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncirculoActionPerformed
        frmcirculo cir = new frmcirculo();
        cir.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btncirculoActionPerformed

    private void btntrapecioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntrapecioActionPerformed
        frmtrapecio tra = new frmtrapecio();
        tra.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btntrapecioActionPerformed

    private void btncircunferenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncircunferenciaActionPerformed
        frmcircunferencia circ = new frmcircunferencia();
        circ.setVisible(true);
        this.setVisible(true);
    }//GEN-LAST:event_btncircunferenciaActionPerformed

   
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmMenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmMenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmMenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmMenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmMenuPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btncerrar;
    private javax.swing.JButton btncirculo;
    private javax.swing.JButton btncircunferencia;
    private javax.swing.JButton btncuadrado;
    private javax.swing.JButton btnminimizar;
    private javax.swing.JButton btnrectangulo;
    private javax.swing.JButton btntrapecio;
    private javax.swing.JButton btntriangulo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
